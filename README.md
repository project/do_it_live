Do it Live
==========

Do it Live is a utility Drupal module. It contains helpers for writing and
running SimpleTest cases intended for active, specific site maintenance (as
opposed to Core/Contrib generic CMS functionality).

Think of it like a poor man's Behat.


### Features

1. Includes a `LiveWebTestCase` class that can be extended for running Web
   test cases against the active database.
2. Includes a drush command file that adds a `drush test-run` command (which is
   almost identical to the command included in drush versions 6.x and earlier).
   Useful for running your tests on a remote pre-production environment.


### Installation

You should almost certainly never enable or use this module on a production
website. Instead, add the module to your codebase and enable it prior to test
runs. Drush is great for this!

```sh
drush dl do_it_live
drush en do_it_live
```


### Usage

Write your SimpleTest classes and cases exactly how you normally would, except
instead of extending `DrupalWebTestCase`, extend `LiveWebTestCase`. All
subsequent uses of `DrupalWebTestCase` helper methods will be run against the
live/active database.  For example:

```php
/**
 * @file
 * You might put this in my_custom_module.test
 */

class MyCustomModuleTestCase extends LiveWebTestCase {

  protected $old_cache_value;

  public function setUp() {
    parent::setUp();

    // Perhaps your tests depend on the cache being disabled.
    $this->old_cache_value = variable_get('cache', FALSE);
    variable_set('cache', FALSE);
  }

  public function tearDown() {
    parent::tearDown();

    // Restore the original cache value.
    variable_set('cache', $this->old_cache_value);
  }

  /**
   * Tests some functionality custom to your site.
   */
  public function testSomeFunctionality() {
    $this->drupalGet('path/to/your/page');
    $this->assertText('Expected text', 'Did not find expected text on page.');
  }

}
```

### Warning

It goes without saying, any changes you make to the site in your tests or
custom setup code will not be automatically cleaned up.  Either write your own
test clean up in your `tearDown` method, or ensure your tests are idempotent.

Above all, be sure you're running your tests against the intended environment.
You probably don't want to run this on production.
